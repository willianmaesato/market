package com.br.leandro.market.features.login.presentation

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.br.leandro.market.R
import com.br.leandro.market.features.components.StandardTextField
import com.br.leandro.market.features.ui.theme.White
import com.br.leandro.market.features.ui.theme.paddingHigh
import com.br.leandro.market.features.ui.theme.paddingMedium
import com.br.leandro.market.features.ui.theme.paddingSmall
import com.br.leandro.market.features.util.navigation.Screen
import kotlinx.coroutines.flow.collect

@Composable
fun LoginScreen(navController: NavController, viewModel: LoginViewModel = hiltViewModel()) {
    LaunchedEffect(key1 = true) {
        viewModel.validationEvents.collect { event ->
            when (event) {
                LoginEvent.Submit -> {
                    navController.popBackStack()
                    navController.navigate(Screen.HomeScreen.route)
                }
            }
        }
    }
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(paddingMedium)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(
                    top = 50.dp,
                    end = paddingMedium,
                    bottom = paddingMedium,
                    start = paddingMedium
                )
                .align(Alignment.Center), verticalArrangement = Arrangement.Top
        ) {
            Text(text = stringResource(id = R.string.login), style = MaterialTheme.typography.h1)
            Spacer(modifier = Modifier.height(paddingMedium))
            StandardTextField(text = viewModel.state.email, onValueChange = {
                viewModel.changeEmail(it)
            }, hint = stringResource(id = R.string.username_hint))
            Spacer(modifier = Modifier.height(paddingSmall))
            StandardTextField(
                text = viewModel.state.password,
                onValueChange = {
                    viewModel.changePassword(it)
                },
                hint = stringResource(id = R.string.password_hint),
                keyboardType = KeyboardType.Password
            )
            Spacer(modifier = Modifier.height(paddingHigh))
            Button(modifier = Modifier
                .height(60.dp)
                .fillMaxWidth()
                .padding(bottom = paddingMedium),
                shape = RoundedCornerShape(8.dp),
                onClick = {
                    viewModel.submitData()
                }
            ) {
                if (viewModel.state.isLoadingButton) {
                    CircularProgressIndicator(color = White, modifier = Modifier.size(24.dp))
                } else {
                    Text(
                        text = stringResource(id = R.string.login),
                        color = MaterialTheme.colors.onPrimary
                    )
                }
            }
        }
        ClickableText(text = buildAnnotatedString {
            append(stringResource(id = R.string.dont_have_an_account_yet))
            append(" ")
            val signUp = stringResource(id = R.string.sign_up)
            withStyle(style = SpanStyle(color = MaterialTheme.colors.primary)) {
                append(signUp)
            }
        },
            style = MaterialTheme.typography.body1,
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(bottom = paddingMedium), onClick = {
                navController.navigate(Screen.RegisterScreen.route)
            })
    }

}