package com.br.leandro.market.features.profile.domain.local

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Profile(
    val id: String,
    val author: String,
    val width: Long,
    val height: Long,
    val url: String,
    val downloadURL: String
) : Parcelable