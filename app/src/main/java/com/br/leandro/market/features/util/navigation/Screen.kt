package com.br.leandro.market.features.util.navigation

sealed class Screen(val route: String){
    object SplashScreen: Screen("splash_screen")
    object LoginScreen: Screen("login_screen")
    object RegisterScreen: Screen("register_screen")
    object HomeScreen: Screen("home_screen")
    object ProfileScreen: Screen("profile_screen")
    object ProfileDetailsScreen: Screen("profile_details_screen")
    object SettingScreen: Screen("setting_screen")
}
