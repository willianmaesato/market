package com.br.leandro.market.features.register.use_case

import com.br.leandro.market.features.register.domain.ValidationResult

class ValidatePassword {

    fun execute(password: String): ValidationResult {
        if (password.length < 8) {
            return ValidationResult(
                successful = false,
                errorMessage = "A senha precisa de pelo menos 8 caracteres"
            )
        }
        val containsLetterAndDigits =
            password.any { it.isDigit() } && password.any { it.isLetter() }
        if (!containsLetterAndDigits) {
            return ValidationResult(
                successful = false,
                errorMessage = "A senha precisa ter pelo menos um número e um digito"
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}