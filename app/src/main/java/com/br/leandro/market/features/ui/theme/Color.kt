package com.br.leandro.market.features.ui.theme

import androidx.compose.ui.graphics.Color

val Blue200 = Color(0xFF90CAF9)
val Blue500 = Color(0xFF2196F3)
val Blue700 = Color(0xFF1976D2)
val Blue900 = Color(0xFF0D47A1)
val LightGray = Color(0xFFf5f5f5)
val Grey200 = Color(0xFFEEEEEE)
val Grey500 = Color(0xFF6D6D6D)
val Grey700 = Color(0xFF404040)
val Black = Color(0xFF000000)
val White = Color(0xFFFFFFFF)
val OrangeYellow1 = Color(0xfff0bd28)
val OrangeYellow2 = Color(0xfff1c746)
val OrangeYellow3 = Color(0xfff4cf65)
val Beige1 = Color(0xfffdbda1)
val Beige2 = Color(0xfffcaf90)
val Beige3 = Color(0xfff9a27b)
val LightGreen1 = Color(0xff54e1b6)
val LightGreen2 = Color(0xff36ddab)
val LightGreen3 = Color(0xff11d79b)
val BlueViolet1 = Color(0xffaeb4fd)
val BlueViolet2 = Color(0xff9fa5fe)
val BlueViolet3 = Color(0xff8f98fd)
val ButtonBlue = Color(0xff505cf3)
