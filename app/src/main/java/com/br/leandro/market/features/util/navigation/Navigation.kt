package com.br.leandro.market.features.util.navigation

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.br.leandro.market.features.home.presentation.HomeScreen
import com.br.leandro.market.features.login.presentation.LoginScreen
import com.br.leandro.market.features.profile.domain.local.Profile
import com.br.leandro.market.features.profile.presentation.ProfileDetailScreen
import com.br.leandro.market.features.profile.presentation.ProfileScreen
import com.br.leandro.market.features.register.presentation.RegisterScreen
import com.br.leandro.market.features.setting.presentation.SettingScreen
import com.br.leandro.market.features.splash.presentation.SplashScreen

@Composable
fun Navigation(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = Screen.SplashScreen.route,
        modifier = Modifier.fillMaxSize()
    ) {
        composable(Screen.SplashScreen.route) {
            SplashScreen(navController = navController)
        }
        composable(Screen.LoginScreen.route) {
            LoginScreen(navController = navController)
        }
        composable(Screen.RegisterScreen.route) {
            RegisterScreen(navController = navController)
        }
        composable(Screen.HomeScreen.route) {
            HomeScreen(navController = navController)
        }
        composable(Screen.ProfileScreen.route) {
            ProfileScreen(navController = navController)
        }
        composable(Screen.ProfileDetailsScreen.route) {
            val profileResult =
                navController.previousBackStackEntry?.savedStateHandle?.get<Profile>("profile")
            profileResult?.let {
                ProfileDetailScreen(navController = navController, profileResult)
            }
        }
        composable(Screen.SettingScreen.route) {
            SettingScreen(navController = navController)
        }
    }
}