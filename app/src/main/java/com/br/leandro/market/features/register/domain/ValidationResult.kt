package com.br.leandro.market.features.register.domain

data class ValidationResult(
    val successful: Boolean,
    val errorMessage: String? = null
)