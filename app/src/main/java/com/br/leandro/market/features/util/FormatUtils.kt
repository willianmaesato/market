package com.br.leandro.market.features.util

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*

fun locale() = Locale("pt", "BR")

object FormatUtils {

    private fun getCurrencyPriceInstance(usePositivePrefix: Boolean): NumberFormat {
        val numberFormat = DecimalFormat("R$ #,##0.00;- R$ ", DecimalFormatSymbols(locale()))
        if (usePositivePrefix) {
            numberFormat.positivePrefix = "+ R$ "
        }
        return numberFormat
    }

    fun formatCurrency(price: Double, usePositivePrefix: Boolean = false): String {
        return getCurrencyPriceInstance(usePositivePrefix).format(price)
    }
}