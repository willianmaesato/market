package com.br.leandro.market.features.register.presentation

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.leandro.market.features.register.domain.RegistrationFormState
import com.br.leandro.market.features.register.use_case.ValidateEmail
import com.br.leandro.market.features.register.use_case.ValidatePassword
import com.br.leandro.market.features.register.use_case.ValidateRepeatedPassword
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val validateEmail: ValidateEmail,
    private val validatePassword: ValidatePassword,
    private val validateRepeatedPassword: ValidateRepeatedPassword
) : ViewModel() {

    var state by mutableStateOf(RegistrationFormState())

    private val validationEventChannel = Channel<RegisterEvent>()
    val validationEvents = validationEventChannel.receiveAsFlow()


    fun changedEmail(email: String) {
        state = state.copy(email = email)
    }

    fun changedPassword(password: String) {
        state = state.copy(password = password)
    }

    fun changedPasswordRepeated(passwordRepeated: String) {
        state = state.copy(repeatPassword = passwordRepeated)
    }

    fun submitData() {
        val emailResult = validateEmail.execute(state.email)
        val passwordResult = validatePassword.execute(state.password)
        val repeatedPasswordResult =
            validateRepeatedPassword.execute(state.password, state.repeatPassword)

        val hasError = listOf(
            emailResult,
            passwordResult,
            repeatedPasswordResult
        ).any { !it.successful }

        if (hasError) {
            state = state.copy(
                emailError = emailResult.errorMessage,
                passwordError = passwordResult.errorMessage,
                repeatPasswordError = repeatedPasswordResult.errorMessage
            )
            return
        }

        viewModelScope.launch {
            validationEventChannel.send(RegisterEvent.Success)
        }
    }
}