package com.br.leandro.market.di

import com.br.leandro.market.features.profile.data.ProfileRemoteDataSource
import com.br.leandro.market.features.profile.data.ProfileRepositoryImpl
import com.br.leandro.market.features.profile.data.ProfileService
import com.br.leandro.market.features.profile.domain.repository.ProfileRepository
import com.br.leandro.market.features.profile.domain.use_case.GetProfileUseCase
import com.br.leandro.market.features.profile.presentation.ProfileViewModel
import com.br.leandro.market.features.register.presentation.RegisterViewModel
import com.br.leandro.market.features.register.use_case.ValidateEmail
import com.br.leandro.market.features.register.use_case.ValidatePassword
import com.br.leandro.market.features.register.use_case.ValidateRepeatedPassword
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import javax.inject.Singleton

@ExperimentalSerializationApi
@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providerRegisterViewModel(): RegisterViewModel {
        return RegisterViewModel(
            validateEmail = ValidateEmail(),
            validatePassword = ValidatePassword(),
            validateRepeatedPassword = ValidateRepeatedPassword()
        )
    }

    @Provides
    @Singleton
    fun providerProfileViewModel(service: ProfileService): ProfileViewModel {
        return ProfileViewModel(
            getProfileUseCase = GetProfileUseCase(
                repository = ProfileRepositoryImpl(
                    remoteDataSource = ProfileRemoteDataSource(
                        service = service
                    )
                )
            )
        )
    }
}