package com.br.leandro.market.features.home.domain.models

import androidx.annotation.DrawableRes
import androidx.compose.ui.graphics.Color

data class HomeCardData(
    val title: String,
    @DrawableRes val iconId: Int,
    val lightColor: Color,
    val mediumColor: Color,
    val darkColor: Color
)
