package com.br.leandro.market.features.profile.presentation

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.br.leandro.market.R
import com.br.leandro.market.features.ui.theme.*
import com.br.leandro.market.features.util.navigation.Screen

@Composable
fun ProfileScreen(
    navController: NavController,
    viewModel: ProfileViewModel = hiltViewModel()
) {
    val state = viewModel.state
    viewModel.loadNextItems()
    Text(
        modifier = Modifier.padding(paddingHigh),
        text = stringResource(id = R.string.profile_screen),
        color = Grey700,
        style = MaterialTheme.typography.h1
    )
    if (state.items.isEmpty()) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator()
        }
    } else {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(bottom = 70.dp, top = 80.dp, start = paddingSmall, end = paddingSmall)
        ) {
            items(state.items.size) { i ->
                val item = state.items[i]
                if (i >= state.items.size - 1 && !state.endReached && !state.isLoading) {
                    viewModel.loadNextItems()
                }
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(80.dp)
                        .padding(paddingSmall)
                        .clip(RoundedCornerShape(paddingSmall))
                        .clickable {
                            navController.currentBackStackEntry?.savedStateHandle?.set(
                                "profile",
                                item
                            )
                            navController.navigate(Screen.ProfileDetailsScreen.route)
                        },
                    elevation = 10.dp,
                    backgroundColor = LightGray
                ) {
                    Column(modifier = Modifier.padding(paddingSmall)) {
                        Text(text = item.id, style = MaterialTheme.typography.body1, color = Black)
                        Spacer(modifier = Modifier.height(paddingSmall))
                        Text(
                            text = item.author,
                            style = MaterialTheme.typography.body1,
                            color = Black
                        )
                    }
                }
            }
            item {
                if (state.isLoading) {
                    Box(
                        modifier = Modifier.fillMaxSize(),
                        contentAlignment = Alignment.BottomCenter
                    ) {
                        CircularProgressIndicator()
                    }
                }
            }
        }
    }
}