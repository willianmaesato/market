package com.br.leandro.market.features.splash.presentation

sealed class SplashEvent {
    object HomeScreen : SplashEvent()
    object LoginScreen : SplashEvent()
}