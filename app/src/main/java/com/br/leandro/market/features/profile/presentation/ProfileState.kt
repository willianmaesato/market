package com.br.leandro.market.features.profile.presentation

import com.br.leandro.market.features.profile.domain.local.Profile

data class ProfileState(
    val isLoading: Boolean = false,
    val items: List<Profile> = listOf(),
    val error: String? = null,
    val endReached: Boolean = false,
    val page: Int = 0
)