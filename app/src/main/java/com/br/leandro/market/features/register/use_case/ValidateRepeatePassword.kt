package com.br.leandro.market.features.register.use_case

import com.br.leandro.market.features.register.domain.ValidationResult

class ValidateRepeatedPassword {

    fun execute(password: String, repeatedPassword: String): ValidationResult {
        if (password != repeatedPassword) {
            return ValidationResult(
                successful = false,
                errorMessage = "As senhas não são iguais"
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}