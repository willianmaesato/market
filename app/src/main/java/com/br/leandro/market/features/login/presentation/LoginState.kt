package com.br.leandro.market.features.login.presentation

data class LoginState(
    val email: String = "",
    val password: String = "",
    val isLoadingButton: Boolean = false
)