package com.br.leandro.market.features.profile.data

import com.br.leandro.market.features.profile.domain.local.Profile
import com.br.leandro.market.features.profile.domain.repository.ProfileRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ProfileRepositoryImpl(private val remoteDataSource: ProfileRemoteDataSource) :
    ProfileRepository {
    override fun getListProfile(page: Int, limit: Int): Flow<List<Profile>> {
        return remoteDataSource.getListProfile(page, limit)
            .map { list -> list.map { it.toDomain() } }
    }
}