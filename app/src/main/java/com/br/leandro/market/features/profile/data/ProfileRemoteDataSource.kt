package com.br.leandro.market.features.profile.data

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class ProfileRemoteDataSource(
    private val service: ProfileService
) {
    fun getListProfile(page: Int, limit: Int): Flow<List<ProfileResponse>> = flow {
        emit(service.getListProfile(page, limit))
    }
}