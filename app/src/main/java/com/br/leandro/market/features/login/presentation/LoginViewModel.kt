package com.br.leandro.market.features.login.presentation

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.leandro.market.data.PreferencesManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val preferencesManager: PreferencesManager
) : ViewModel() {

    var state by mutableStateOf(LoginState())

    private val validationEventChannel = Channel<LoginEvent>()
    val validationEvents = validationEventChannel.receiveAsFlow()

    fun changeEmail(email: String) {
        state = state.copy(email = email)
    }

    fun changePassword(password: String) {
        state = state.copy(password = password)
    }

    fun submitData() {
        if (state.isLoadingButton) {
            return
        }
        state = state.copy(isLoadingButton = true)
        viewModelScope.launch {
            delay(5000)
            preferencesManager.updateUserName(state.email)
            validationEventChannel.send(LoginEvent.Submit)
        }
    }
}