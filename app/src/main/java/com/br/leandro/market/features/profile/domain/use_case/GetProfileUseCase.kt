package com.br.leandro.market.features.profile.domain.use_case

import com.br.leandro.market.features.profile.domain.local.Profile
import com.br.leandro.market.features.profile.domain.repository.ProfileRepository
import kotlinx.coroutines.flow.Flow

class GetProfileUseCase(private val repository: ProfileRepository) {
    operator fun invoke(page: Int, limit: Int): Flow<List<Profile>> =
        repository.getListProfile(page, limit)

}