package com.br.leandro.market.features.register.use_case

import android.util.Patterns
import com.br.leandro.market.features.register.domain.ValidationResult

class ValidateEmail {

    fun execute(email: String): ValidationResult {
        if (email.isBlank()) {
            return ValidationResult(
                successful = false,
                errorMessage = "O E-mail não pode ser em branco"
            )
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return ValidationResult(
                successful = false,
                errorMessage = "Este E-mail não é valido"
            )
        }

        return ValidationResult(
            successful = true
        )
    }
}