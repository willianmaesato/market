package com.br.leandro.market.data

import android.content.Context
import android.util.Log
import androidx.datastore.createDataStore
import com.br.leandro.market.domain.models.UserData
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "Preferences Manager"

@Singleton
class PreferencesManager @Inject constructor(@ApplicationContext context: Context) {

    private val dataStore = context.createDataStore("MarketPreferences", UserSerializer)

    val userDataInit = dataStore.data
        .map { preferences ->
            preferences.copy(name = preferences.name, email = preferences.email)
        }.catch { exception ->
            if (exception is IOException) {
                Log.e(TAG, "Error reading preferences", exception)
                emit(UserData())
            } else {
                throw exception
            }
        }

    suspend fun updateUserName(userName: String) {
        dataStore.updateData { userData ->
            userData.copy(name = userName)
        }
    }
}