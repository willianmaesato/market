package com.br.leandro.market.features.home.presentation

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.br.leandro.market.R
import com.br.leandro.market.features.home.components.BalanceCardItem
import com.br.leandro.market.features.home.components.HomeCardItem
import com.br.leandro.market.features.home.domain.models.HomeCardData
import com.br.leandro.market.features.ui.theme.*

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun HomeScreen(
    navController: NavController,
    viewModel: HomeViewModel = hiltViewModel()
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = LightGray)
    ) {
        Text(
            modifier = Modifier
                .padding(paddingHigh)
                .align(Alignment.CenterHorizontally),
            text = stringResource(id = R.string.welcome, viewModel.getUserName()),
            color = Grey700,
            style = MaterialTheme.typography.h2,
            fontSize = 18.sp
        )
        BalanceCardItem(4000.0)
        val listHome = listOf(
            HomeCardData(
                title = "Abrir Caixa",
                R.drawable.ic_market,
                BlueViolet1,
                BlueViolet2,
                BlueViolet3
            ),
            HomeCardData(
                title = "Fechar Caixa",
                R.drawable.ic_market,
                LightGreen1,
                LightGreen2,
                LightGreen3
            ),
            HomeCardData(
                title = "Buscar Preço",
                R.drawable.ic_market,
                OrangeYellow1,
                OrangeYellow2,
                OrangeYellow3
            ),
            HomeCardData(
                title = "Realizar Vendas",
                R.drawable.ic_market,
                Beige1,
                Beige2,
                Beige3
            )
        )
        LazyVerticalGrid(
            cells = GridCells.Fixed(2),
            contentPadding = PaddingValues(start = 7.5.dp, end = 7.5.dp, bottom = 100.dp),
            modifier = Modifier.fillMaxHeight()
        ) {
            items(listHome.size) {
                HomeCardItem(homeCard = listHome[it])
            }
        }
    }
}