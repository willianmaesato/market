package com.br.leandro.market.features.home.presentation

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.leandro.market.data.PreferencesManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val preferencesManager: PreferencesManager
) : ViewModel() {

    private var state by mutableStateOf(HomeState())

    init {
        getListHomeCard()
    }

    fun getUserName(): String = state.userName

    private fun getListHomeCard() {
        viewModelScope.launch {
            preferencesManager.userDataInit.collect { userName ->
                updateUserName(userName.name)
            }
        }
    }

    private fun updateUserName(userName: String) {
        state = state.copy(userName = userName)
    }
}