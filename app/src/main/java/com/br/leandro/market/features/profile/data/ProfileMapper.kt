package com.br.leandro.market.features.profile.data

import com.br.leandro.market.features.profile.domain.local.Profile

fun ProfileResponse.toDomain(): Profile {
    return Profile(
        id = id,
        author = author,
        width = width,
        height = height,
        url = url,
        downloadURL = downloadURL
    )
}