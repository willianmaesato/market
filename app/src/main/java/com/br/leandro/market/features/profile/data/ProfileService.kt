package com.br.leandro.market.features.profile.data

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ProfileService {

    @GET("v2/list?page=10&limit=100")
    suspend fun getListProfile(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): List<ProfileResponse>
}