package com.br.leandro.market.features.profile.data

import kotlinx.serialization.*

@Serializable
data class ProfileResponse (
    val id: String,
    val author: String,
    val width: Long,
    val height: Long,
    val url: String,

    @SerialName("download_url")
    val downloadURL: String
)
