package com.br.leandro.market.features.util.paginator

interface Paginator<Key, Item> {
    suspend fun loadNextItems()
    fun reset()
}