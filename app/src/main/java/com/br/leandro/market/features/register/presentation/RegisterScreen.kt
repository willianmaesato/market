package com.br.leandro.market.features.register.presentation

import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.br.leandro.market.R
import com.br.leandro.market.features.components.StandardTextField
import com.br.leandro.market.features.ui.theme.paddingHigh
import com.br.leandro.market.features.ui.theme.paddingMedium
import com.br.leandro.market.features.ui.theme.paddingSmall
import kotlinx.coroutines.flow.collect

@Composable
fun RegisterScreen(
    navController: NavController, viewModel: RegisterViewModel = hiltViewModel()
) {
    val state = viewModel.state
    val context = LocalContext.current
    LaunchedEffect(key1 = context) {
        viewModel.validationEvents.collect { event ->
            when (event) {
                RegisterEvent.Success -> {
                    Toast.makeText(context, "Registrado com sucesso", Toast.LENGTH_LONG).show()
                    navController.popBackStack()
                }
            }
        }
    }
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(paddingMedium)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(
                    top = 50.dp,
                    end = paddingMedium,
                    bottom = paddingMedium,
                    start = paddingMedium
                )
                .align(Alignment.Center), verticalArrangement = Arrangement.Top
        ) {
            Text(text = stringResource(id = R.string.register), style = MaterialTheme.typography.h1)
            Spacer(modifier = Modifier.height(paddingMedium))
            StandardTextField(
                text = state.email, onValueChange = {
                    viewModel.changedEmail(it)
                },
                error = state.emailError,
                hint = stringResource(id = R.string.username_hint),
                keyboardType = KeyboardType.Email
            )
            Spacer(modifier = Modifier.height(paddingSmall))
            StandardTextField(
                text = state.password,
                onValueChange = {
                    viewModel.changedPassword(it)
                },
                error = state.passwordError,
                hint = stringResource(id = R.string.password_hint),
                keyboardType = KeyboardType.Password
            )
            Spacer(modifier = Modifier.height(paddingSmall))
            StandardTextField(
                text = state.repeatPassword,
                onValueChange = {
                    viewModel.changedPasswordRepeated(it)
                },
                error = state.repeatPasswordError,
                hint = stringResource(id = R.string.repeated_password_hint),
                keyboardType = KeyboardType.Password
            )
            Spacer(modifier = Modifier.height(paddingHigh))
            Button(modifier = Modifier
                .height(60.dp)
                .fillMaxWidth()
                .padding(bottom = paddingMedium),
                shape = RoundedCornerShape(8.dp),
                onClick = {
                    viewModel.submitData()
                }
            ) {
                Text(
                    text = stringResource(id = R.string.register),
                    color = MaterialTheme.colors.onPrimary
                )
            }
        }
        Text(
            text = buildAnnotatedString {
                append(stringResource(id = R.string.already_have_an_account))
                append(" ")
                val signUpText = stringResource(id = R.string.sign_in)
                withStyle(
                    style = SpanStyle(
                        color = MaterialTheme.colors.primary
                    )
                ) {
                    append(signUpText)
                }
            },
            style = MaterialTheme.typography.body1,
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(bottom = paddingMedium)
                .clickable {
                    navController.popBackStack()
                }
        )
    }
}