package com.br.leandro.market.features.login.presentation

sealed class LoginEvent {
    object Submit : LoginEvent()
}