package com.br.leandro.market.features.setting.presentation

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import com.br.leandro.market.R
import com.br.leandro.market.features.ui.theme.Grey700
import com.br.leandro.market.features.ui.theme.paddingHigh

@Composable
fun SettingScreen(
    navController: NavController
) {
    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            modifier = Modifier.padding(paddingHigh),
            text = stringResource(id = R.string.setting_screen),
            color = Grey700,
            style = MaterialTheme.typography.h1
        )
    }
}