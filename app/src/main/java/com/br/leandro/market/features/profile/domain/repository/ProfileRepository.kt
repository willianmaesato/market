package com.br.leandro.market.features.profile.domain.repository

import com.br.leandro.market.features.profile.domain.local.Profile
import kotlinx.coroutines.flow.Flow

interface ProfileRepository {
    fun getListProfile(page: Int, limit: Int): Flow<List<Profile>>
}