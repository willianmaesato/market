package com.br.leandro.market.domain.models

import kotlinx.serialization.Serializable

@Serializable
data class UserData(
    val name: String = "",
    val email: String = ""
)
