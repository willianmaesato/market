package com.br.leandro.market.features.home.presentation

import com.br.leandro.market.features.home.domain.models.HomeCardData

data class HomeState(
    val listHomeCard: List<HomeCardData> = emptyList(),
    val isLoading: Boolean = false,
    val userName: String = ""
)