package com.br.leandro.market.features.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.br.leandro.market.R

val sans = FontFamily(
    Font(R.font.sans_semibold, FontWeight.SemiBold),
    Font(R.font.sans_light, FontWeight.Light),
    Font(R.font.sans_bold, FontWeight.Bold),
    Font(R.font.sans_regular, FontWeight.Normal),
)

val Typography = Typography(
    body1 = TextStyle(
        fontFamily = sans,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp,
        color = Grey500
    ),
    h1 = TextStyle(
        fontFamily = sans,
        fontWeight = FontWeight.Bold,
        fontSize = 30.sp,
        color = Blue500
    ),
    h2 = TextStyle(
        fontFamily = sans,
        fontWeight = FontWeight.SemiBold,
        fontSize = 24.sp,
        color = Blue500
    ),
    body2 = TextStyle(
        fontFamily = sans,
        fontWeight = FontWeight.Light,
        fontSize = 12.sp,
        color = Blue500
    )
)