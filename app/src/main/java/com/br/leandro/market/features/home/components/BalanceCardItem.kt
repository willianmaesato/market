package com.br.leandro.market.features.home.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.br.leandro.market.R
import com.br.leandro.market.features.ui.theme.Blue500
import com.br.leandro.market.features.ui.theme.White
import com.br.leandro.market.features.ui.theme.paddingMedium
import com.br.leandro.market.features.util.FormatUtils

@Composable
fun BalanceCardItem(price: Double = 0.0) {
    var isBalanceVisible by remember {
        mutableStateOf(false)
    }

    val priceFormat = FormatUtils.formatCurrency(price)

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = paddingMedium, bottom = paddingMedium, end = paddingMedium)
            .height(80.dp)
            .clip(RoundedCornerShape(10.dp))
            .background(Blue500)
            .clickable {
                isBalanceVisible = !isBalanceVisible
            }
    ) {
        Text(
            modifier = Modifier
                .padding(paddingMedium)
                .align(Alignment.TopCenter),
            text = stringResource(id = R.string.cash_balance),
            color = White,
            style = MaterialTheme.typography.body1,
            fontSize = 14.sp
        )
        Icon(
            imageVector = if (isBalanceVisible) Icons.Filled.VisibilityOff
            else Icons.Filled.Visibility,
            tint = White,
            contentDescription = "",
            modifier = Modifier
                .align(Alignment.CenterStart)
                .padding(paddingMedium)
        )
        Text(
            modifier = Modifier
                .padding(paddingMedium)
                .align(Alignment.BottomCenter),
            text = if (isBalanceVisible) priceFormat
            else stringResource(id = R.string.cash_balance_hide),
            color = White,
            style = MaterialTheme.typography.h1,
            fontSize = 18.sp
        )
    }
}