package com.br.leandro.market.features.register.presentation

sealed class RegisterEvent {
    object Success : RegisterEvent()
}