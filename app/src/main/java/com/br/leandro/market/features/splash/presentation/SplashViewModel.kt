package com.br.leandro.market.features.splash.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.leandro.market.data.PreferencesManager
import com.br.leandro.market.util.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val preferencesManager: PreferencesManager
) : ViewModel() {

    private val validationEventChannel = Channel<SplashEvent>()
    val validationEvents = validationEventChannel.receiveAsFlow()

    init {
        viewModelScope.launch {
            preferencesManager.userDataInit
                .flowOn(Dispatchers.IO)
                .onStart { }
                .catch { }
                .onCompletion { }.collect { response ->
                    delay(Constants.SPLASH_SCREEN_DURATION)
                    if (response.name.isNotEmpty()) {
                        validationEventChannel.send(SplashEvent.HomeScreen)
                    } else {
                        validationEventChannel.send(SplashEvent.LoginScreen)
                    }
                }
        }
    }
}