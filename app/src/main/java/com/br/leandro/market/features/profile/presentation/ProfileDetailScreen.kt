package com.br.leandro.market.features.profile.presentation

import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import com.br.leandro.market.features.profile.domain.local.Profile
import com.br.leandro.market.features.ui.theme.Grey700
import com.br.leandro.market.features.ui.theme.paddingHigh

@Composable
fun ProfileDetailScreen(navController: NavController, profile: Profile) {
    Text(
        modifier = Modifier.padding(paddingHigh),
        text = "${profile.id}\n${profile.author}",
        color = Grey700,
        style = MaterialTheme.typography.h1
    )
}